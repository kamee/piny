#include <stdio.h>

struct User {
    int age;
    float weight;
};

int get_data() {
    // TODO: add error handling
    // TODO: write to struct
    int age;
    float weight;

    printf("input your age please: ");
    scanf("%d", &age);
    printf("%d\n", age);

    // TODO: fix floating point
    printf("print your weight please: ");
    scanf("%f", &weight);
    printf("%f\n", weight);

    // TODO: change hardcoded part
    // TODO: pass data to struct
    return 52;
}

int calculate_bmr(){
//    W, BMR = 655.1 + (9.563 x weight in kg) + (1.850 x height in cm) - (4.676 x age in years)
//    M, BMR = 66.47 + (13.75 x weight in kg) + (5.003 x height in cm) - (6.755 x age in years)
}

int main (void) {
    get_data();
    calculate_bmr();

    return 0;
}
